'''
Copyright (c) 2020 Jack Poulson <jack@techinquiry.org>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

PyNLRB is a simple scraper for retrieving cases from https://www.nlrb.gov
using BeautifulSoup. There are four primary use cases at the moment:

  1. Retrieving all cases matching a particular query string, in the form of
     a dictionary mapping the relevant case numbers to their case info. E.g.,
     cases mentioning 'google' can be retrieved via:

     >>> import pynlrb
     >>> cases = pynlrb.query_cases('google')

  2. Retrieving a particular case from its case number:

     >>> case = pynlrb.case('02-CA-266980')

  3. Transform a list of case numbers into a dictionary mapping their case
     numbers to their case info.

     >>> cases = pynlrb.cases(['02-CA-266980', ...])

  4. Constructing a pandas.DataFrame of the summaries of recent filings:

     >>> df = pynlrb.recent_cases_dataframe()

Please keep in mind that the NLRB server is extremely slow.

Further, there is a well-known memory leak in lxml that limits the number of
independent NLRB cases that can be scraped in a single process. See

    https://groups.google.com/g/beautifulsoup/c/ZCEsPeTQjLk

for more information.

TODO(Jack Poulson):

 - Add a scraper for the cases mentioned on the Board Decisions page:

     https://www.nlrb.gov/cases-decisions/decisions/board-decisions

'''
import bs4
import csv
import gc
import io
import json
import pandas as pd
import requests
from bs4 import BeautifulSoup

BASE_SEARCH_URL = 'https://www.nlrb.gov/search/case'
BASE_CASE_URL = 'https://www.nlrb.gov/case'
RECENT_CASES_CSV_URL = 'https://www.nlrb.gov/reports/graphs-data/recent-filings/csv-export'

# If no bytes are received by requests.get for the following number of seconds,
# then an exception will be thrown.
DEFAULT_TIMEOUT = 30.0

# Using 'lxml' instead of 'html.parser' is critical for accurately
# parsing tables. Otherwise we see spurious patterns such as
# '</tr></tr></tr>...'.
PARSER = 'lxml'


def query_case_summaries_url(query, page_number=0):
    '''Returns the URL for the given page of a query's case summaries.'''
    return '{}/{}?page={}'.format(BASE_SEARCH_URL, query, page_number)


def case_url(case_number):
    '''Returns the URL for retrieving the case with the given case number.'''
    return '{}/{}'.format(BASE_CASE_URL, case_number)


def query_case_summaries_soup(query, page_number=0, timeout=DEFAULT_TIMEOUT):
    '''Returns a soup of summaries for the given query and page number.'''
    url = query_case_summaries_url(query, page_number)
    try:
        page = requests.get(url, timeout=timeout)
        return BeautifulSoup(page.content, PARSER)
    except:
        return None


def case_soup(case_number, timeout=DEFAULT_TIMEOUT):
    '''Returns a case soup for the given case number.'''
    url = case_url(case_number)
    try:
        page = requests.get(url, timeout=timeout)
        return BeautifulSoup(page.content, PARSER)
    except:
        return None


def num_total_case_summaries_from_soup(soup):
    '''Returns number of total case summaries from a search BeautifulSoup.'''
    total_results_num = soup.find(id='total_results_num')
    if total_results_num == None:
        total_results = 0
    else:
        total_results = int(total_results_num.text)
    return total_results


def query_case_summaries_from_soup(soup):
    '''Returns an array of case summaries from a search BeautifulSoup.'''
    if soup == None:
        return []
    CASE_NUMBER_LABEL = 'Case Number: '
    DATE_FILED_LABEL = 'Date Filed: '
    STATUS_LABEL = 'Status: '
    LOCATION_LABEL = 'Location: '
    REGION_ASSIGNED_LABEL = 'Region Assigned: '

    summaries = []
    for result in soup.find_all(class_='partition-div'):
        left_div = result.contents[1]
        if left_div.contents[1].text == CASE_NUMBER_LABEL:
            case_number = left_div.contents[2].contents[0]
        else:
            print('Did not find case number label in position 1.')
            print('left_div: {}'.format(left_div.contents))
            case_number = None
        if left_div.contents[5].text == DATE_FILED_LABEL:
            date_filed = left_div.contents[6]
        else:
            print('Did not find date filed label in position 5.')
            print('left_div: {}'.format(left_div.contents))
            date_filed = None
        if left_div.contents[9].text == STATUS_LABEL:
            status = left_div.contents[10]
        else:
            print('Did not find status label in position 9.')
            print('left_div: {}'.format(left_div.contents))
            status = None

        right_div = result.contents[3]
        if right_div.contents[1].text == LOCATION_LABEL:
            location = right_div.contents[2]
        else:
            print('Did not find location label in position 1.')
            print('right_div: {}'.format(right_div.contents))
            location = None
        if right_div.contents[5].text == REGION_ASSIGNED_LABEL:
            region_assigned = right_div.contents[6]
        else:
            print('Did not find region assigned label in position 5.')
            print('right_div: {}'.format(right_div.contents))
            region_assigned = None

        summaries.append({
            'case_number': case_number,
            'date_filed': date_filed,
            'status': status,
            'location': location,
            'region_assigned': region_assigned
        })

    return summaries


def case_from_soup(soup):
    '''Returns a dictionary summarizing a case from its BeautifulSoup.'''
    if soup == None:
        return None

    case = {}

    # The format of each participant table portion should be an array of form:
    #
    # 0: '\n'
    # 1:
    #  '''
    #  <td><b> Charged Party / Respondent</b><br/>Legal Representative<br/>
    #                              Dhupar, Ankush<br/>
    #                                          Paul Hasting LLP<br/>
    #  </td>
    #  '''
    # 2: '\n'
    # 3:
    #  '''
    #  <td>
    #                              515 SOUTH FLOWER STREET, 25TH FLOOR<br/>
    #                                                      LOS ANGELES, CA<br/>
    #                                          90071-2228<br/>
    #  </td>
    #  '''
    # 4: '\n'
    # 5:
    #  '''
    #  <td>
    #                              (213)683-6000<br/>
    #  </td>
    #  '''
    # 6: '\n'
    #
    PARTICIPANT_LENGTH = 7

    # In index 1 of the above participant table portion array, we should have
    # another array of length 9, 7, or 5 of one of the following forms:
    #
    # 0: '<b> Charged Party / Respondent</b>'
    # 1: '<br/>'
    # 2: 'Legal Representative'
    # 3: '<br/>'
    # 4: '\n                     Dhupar, Ankush'
    # 5: '<br/>'
    # 6: '\n                     Paul Hasting LLP'
    # 7: '\n'
    # 8: '<br/>'
    #
    # 0: '<b> Charging Party</b>'
    # 1: '<br/>'
    # 2: 'Individual'
    # 3: '<br/>'
    # 4: '\n'
    #
    # 0: '<b> Charged Party / Respondent</b>'
    # 1: '<br/>'
    # 2: 'Employer'
    # 3: '<br/>'
    # 4: '\n                    GOOGLE, INC., A SUBSIDIARY OF ALPHABET INC.'
    # 5: '<br/>'
    # 6: '\n'
    #
    PARTICIPANT_TYPE_INDEX = 1

    # In index 3 of the above participant table portion array, we should have
    # another array with a subset of the following form:
    #
    # 0: '\n                  515 SOUTH FLOWER STREET'
    # 1: '<br/>
    # 2: '\n                  25TH FLOOR'
    # 3: '<br/>
    # 4: '\n                  LOS ANGELES, CA'
    # 5: '<br/>
    # 6: '\n                  90071-2228'
    # 7: '<br/>
    # 8: '\n'
    #
    # If the array is of length 9, then we assume the existence of both a
    # street address and a secondary street address line. If of length 7,
    # we ignore the secondary street address, and if of length 5, we also
    # ignore the street address. If of length 3, we only read in the city/state.
    PARTICIPANT_ADDRESS_INDEX = 3

    # In index 5 of the above participant table portion array, we should have
    # another array of length 3 of the form:
    #
    # 0: '\n                 (213)683-6000'
    # 1: '<br/>'
    # 2: '\n'
    #
    PARTICIPANT_PHONE_INDEX = 5

    # Read in the header information.
    for header_div in soup.find_all(class_='partition-div', limit=1):
        header_left_div = header_div.contents[1]
        case['case_number'] = header_left_div.contents[2].strip()
        case['date_filed'] = header_left_div.contents[7].text.strip()
        case['status'] = header_left_div.contents[11].strip()

        header_right_div = header_div.contents[3]
        case['location'] = header_right_div.contents[2].strip()
        case['region_assigned'] = header_right_div.contents[6].strip()
        if len(header_right_div.contents) > 10:
            case['reason_closed'] = header_right_div.contents[10].strip()
        else:
            case['reason_closed'] = None

    # Read in the docket activity.
    case['docket_activity'] = []
    for docket_table in soup.find_all('table',
                                      class_='docket-activity-table',
                                      limit=1):
        table_body = docket_table.contents[3]
        table_body_len = len(table_body.contents)
        for index in range(1, table_body_len, 2):
            item = table_body.contents[index]
            date = item.contents[1].text.strip()
            document_type = item.contents[3].text.strip()
            filed_by = item.contents[5].text.strip()
            case['docket_activity'].append({
                'date': date,
                'document_type': document_type,
                'filed_by': filed_by
            })

    # Read in the related documents. We check that the found unordered list is
    # not actually for the list of allegations.
    #
    # TODO(Jack Poulson): Find how to extract document positions of the found
    # elements so that we can compare them against those of landmarks to
    # check if we are in the appropriate section.
    #
    case['related_documents'] = []
    for related_documents_ul in soup.find(
            'h2', text='Related Documents').find_next_siblings('ul', limit=1):
        for allegations_ul in soup.find(
                'h2', text='Allegations').find_next_siblings('ul', limit=1):
            if related_documents_ul == allegations_ul:
                continue
            related_documents_ul_len = len(related_documents_ul.contents)
            if related_documents_ul_len >= 2:
                for index in range(1, related_documents_ul_len, 2):
                    related_document = related_documents_ul.contents[index]
                    try:
                        url = related_document.contents[0]['href']
                        title = related_document.contents[0].text.strip()
                        case['related_documents'].append({
                            'url': url,
                            'title': title
                        })
                    except:
                        print(related_document.contents)
                        print(related_document.contents[0])

    # Read in the allegations.
    case['allegations'] = []
    for allegations_ul in soup.find(
            'h2', text='Allegations').find_next_siblings('ul', limit=1):
        allegations_ul_len = len(allegations_ul.contents)
        if allegations_ul_len >= 2:
            for index in range(1, allegations_ul_len, 2):
                allegation = allegations_ul.contents[index]
                if type(allegation) is bs4.element.NavigableString:
                    item = allegation
                else:
                    item = allegation.text
                case['allegations'].append(item)

    # Read in the participants.
    case['participants'] = []
    for participants_table in soup.find_all('table',
                                            class_='Participants',
                                            limit=1):
        table_body = participants_table.contents[3]
        table_body_len = len(table_body.contents)
        for body_index in range(1, table_body_len):
            participant_portion = table_body.contents[body_index]
            participant_portion_len = len(participant_portion.contents)
            if participant_portion_len != PARTICIPANT_LENGTH:
                print('participant_portion_len: {}'.format(
                    participant_portion_len))
                continue

            participant = {}

            # Parse the participant type information.
            participant_type = participant_portion.contents[
                PARTICIPANT_TYPE_INDEX]
            participant_type_len = len(participant_type.contents)
            type_index = 0
            participant['charging_type'] = None
            participant['employment_type'] = None
            participant['name'] = None
            participant['employer'] = None
            for content in participant_type.contents:
                if content.name == 'br':
                    type_index += 1
                    continue
                elif content == '\n':
                    continue

                if type_index == 0:
                    participant['charging_type'] = content.text.strip()
                elif type_index == 1:
                    participant['employment_type'] = content
                elif type_index == 2:
                    participant['name'] = content.strip()
                elif type_index == 3:
                    participant['employer'] = content.strip()
                else:
                    print('participant type index: {}, content: {}'.format(
                        type_index, content))

            # Parse the participant address information.
            participant_address = participant_portion.contents[
                PARTICIPANT_ADDRESS_INDEX]
            participant_address_len = len(participant_address.contents)
            participant['street_address'] = None
            participant['city_and_state'] = None
            participant['zipcode'] = None
            if (participant_address_len == 9 or participant_address_len == 7
                    or participant_address_len == 5
                    or participant_address_len == 3):
                index = 0
                contents = participant_address.contents
                if (participant_address_len == 9
                        or participant_address_len == 7):
                    participant['street_address'] = contents[index].strip()
                    index += 2
                    if participant_address_len == 9:
                        participant['street_address'] = '{}, {}'.format(
                            participant['street_address'],
                            contents[index].strip())
                        index += 2
                participant['city_and_state'] = contents[index].strip()
                index += 2
                if participant_address_len > 3:
                    participant['zipcode'] = contents[index].strip()
                    index += 2
            else:
                if participant_address_len > 1:
                    print('participant_address_len: {}'.format(
                        participant_address_len))
                    for index in range(participant_address_len):
                        piece = participant_address.contents[index]
                        print('  {}: {}'.format(index, piece))

            # Parse the participant phone information.
            participant_phone = participant_portion.contents[
                PARTICIPANT_PHONE_INDEX]
            participant_phone_len = len(participant_phone.contents)
            if participant_phone_len == 3:
                participant['phone_number'] = participant_phone.contents[
                    0].strip()
            else:
                participant['phone_number'] = None
                if participant_phone_len > 1:
                    print('participant_phone_len: {}'.format(
                        participant_phone_len))
                    for index in range(participant_phone_len):
                        piece = participant_phone.contents[index]
                        print('  {}: {}'.format(index, piece))

            case['participants'].append(participant)

    # Read in the related cases.
    case['related_cases'] = []
    for related_cases_table in soup.find_all('table',
                                             class_='related-case',
                                             limit=1):
        table_body = related_cases_table.contents[3]
        table_body_len = len(table_body.contents)
        for index in range(1, table_body_len, 2):
            related_case = table_body.contents[index]
            case_number = related_case.contents[1].contents[1].text
            case_name = related_case.contents[3].text.strip()
            status = related_case.contents[5].text.strip()
            case['related_cases'].append({
                'case_number': case_number,
                'case_name': case_name,
                'status': status
            })

    return case


def query_case_summaries(query,
                         page_number=None,
                         timeout=DEFAULT_TIMEOUT,
                         verbose=True):
    '''Returns an array of case summaries for the given query and page.'''
    SUMMARIES_PER_PAGE = 10

    def ceildiv(a, b):
        return -(-a // b)

    summaries = []
    if page_number == None:
        soup = query_case_summaries_soup(query, page_number=0, timeout=timeout)
        if soup == None:
            return []
        num_total_summaries = num_total_case_summaries_from_soup(soup)
        num_pages = ceildiv(num_total_summaries, SUMMARIES_PER_PAGE)
        if verbose:
            print('Will retrieve {} summaries across {} pages.'.format(
                num_total_summaries, num_pages))
        for page in range(num_pages):
            if verbose:
                print('  Retrieving summary page {} of {}'.format(
                    page, num_pages))
            summaries += query_case_summaries(query,
                                              page_number=page,
                                              timeout=timeout,
                                              verbose=verbose)
        soup.decompose()
        del soup
        gc.collect()
    else:
        soup = query_case_summaries_soup(query,
                                         page_number=page_number,
                                         timeout=timeout)
        summaries = query_case_summaries_from_soup(soup)
        soup.decompose()
        del soup
        gc.collect()
    return summaries


def recent_cases_dataframe(timeout=DEFAULT_TIMEOUT):
    '''Returns a DataFrame representation of recent cases CSV.'''
    try:
        page = requests.get(RECENT_CASES_CSV_URL, timeout=timeout)
        df = pd.read_csv(io.StringIO(page.content.decode('utf-8')))
        df['Date Filed'] = pd.to_datetime(df['Date Filed'], format='%m/%d/%Y')
        return df
    except:
        print('Timed out on retrieving {}'.format(RECENT_CASES_CSV_URL))
        return None


def case(case_number, timeout=DEFAULT_TIMEOUT):
    '''Returns a dictionary for the case with the given case number.'''
    case_info = case_from_soup(case_soup(case_number, timeout=timeout))
    gc.collect()
    return case_info


def cases(case_numbers, timeout=DEFAULT_TIMEOUT, verbose=True):
    '''Returns a dictionary for each case with corresponding case number.'''
    num_cases = len(case_numbers)
    case_dict = {}
    for case_index in range(num_cases):
        case_number = case_numbers[case_index]
        if verbose:
            print('Retrieving case {} ({} of {})'.format(
                case_number, case_index, num_cases))
        case_dict[case_number] = case(case_number, timeout=timeout)
    return case_dict


def query_cases(query, timeout=DEFAULT_TIMEOUT, verbose=True):
    '''Returns the cases associated with a particular query.'''
    summaries = query_case_summaries(query, timeout=timeout, verbose=verbose)
    num_cases = len(summaries)
    case_dict = {}
    for case_index in range(num_cases):
        summary = summaries[case_index]
        case_number = summary['case_number']
        if verbose:
            print('Retrieving case {} ({} of {})'.format(
                case_number, case_index, num_cases))
        case_dict[case_number] = case(case_number, timeout=timeout)
    return case_dict


def participants_set(cases):
    '''Converts a dictionary from case numbers to cases to participants set.'''
    participants = set()
    for case_number in cases:
        case_info = cases[case_number]
        for participant in case_info['participants']:
            if 'name' in participant and participant['name'] != None:
                participants.add(participant['name'].lower())
    return participants


def json_to_csv(json_filename, csv_filename):
    '''Converts a JSON representation of cases into a CSV.'''
    df = pd.read_json(json_filename)
    df['date_filed'] = pd.to_datetime(
        df['date_filed'], format='%m/%d/%Y', errors='coerce')

    df['allegations'] = df['allegations'].apply(
        lambda value : json.dumps(value))
    df['participants'] = df['participants'].apply(
        lambda value : json.dumps(value).lower())
    df['docket_activity'] = df['docket_activity'].apply(
        lambda value : json.dumps(value))
    df['related_cases'] = df['related_cases'].apply(
        lambda value : json.dumps(value))
    df['related_documents'] = df['related_documents'].apply(
        lambda value : json.dumps(value))

    # We must use a tab separator and avoid newlines or MySQL will truncate
    # all text inputs to 255 characters. This causes JSON parsing issues for
    # some of Google's "lobbyist_positions" fields.

    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    duplicated = df[['case_number']].duplicated(keep='first')
    num_dropped = sum(duplicated)
    print('Dropping {} redundant rows.'.format(num_dropped))
    df = df.drop(df[duplicated].index)

    df.to_csv(csv_filename,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)

