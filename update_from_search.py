import argparse
import json
import pynlrb


CASES_SAVE_FILE = 'cases.json'


parser = argparse.ArgumentParser(description='Query NLRB cases')
parser.add_argument('--query', help='The search query')
args = parser.parse_args()

# Load the pre-existing cases data.
cases_save = json.load(open(CASES_SAVE_FILE))

print('Querying for "{}"'.format(args.query))
new_cases = pynlrb.query_cases(args.query)

for case_number in new_cases:
    case_info = new_cases[case_number]
    if case_info != None:
        if case_number in cases_save: 
            if cases_save[case_number] == case_info:
                print('  Parse of case {} was identical to save.'.format(
                    case_number))
            else:
                print('  Updating case {}'.format(case_number))
                cases_save[case_number] = case_info
        else:
            print('  Inserting case {}'.format(case_number))
            cases_save[case_number] = case_info

# Now save the updated cases.
with open(CASES_SAVE_FILE, 'w') as outfile:
    json.dump(cases_save, outfile, indent=1)
