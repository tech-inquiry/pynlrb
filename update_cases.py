import json
import pynlrb
import random


# lxml seems to have a significant memory leak due to its global dictionary.
MAX_PER_PROCESS_PARSES = 500

CASES_SAVE_FILE = 'cases.json'

# Load the pre-existing cases data.
cases_save = json.load(open(CASES_SAVE_FILE))


# Pull in the cases to process.
df = pynlrb.recent_cases_dataframe()
case_numbers = df['Case Number']
case_numbers_list = list(case_numbers)


num_parses_left = MAX_PER_PROCESS_PARSES
updated_case_numbers = {}


# TODO(Jack Poulson): Add sorting to prevent detection of reordered lists as
# different. Maybe use a set instead.

# Prioritize getting the new cases processed.
num_skipped = 0
for case_number in case_numbers_list:
    if num_parses_left == 0:
        break
    if case_number in cases_save and cases_save[case_number] != None:
        num_skipped += 1
        continue
    if num_skipped > 0:
        print('  Skipped {} already-retrieved cases.'.format(num_skipped))
        num_skipped = 0
    print('Processing case {} ({} parses left)'.format(
        case_number, num_parses_left))
    case_info = pynlrb.case(case_number)
    if case_info != None:
        updated_case_numbers[case_number] = True
        cases_save[case_number] = case_info
    num_parses_left -= 1    


related_cases = set()
for case_number in cases_save:
    case_info = cases_save[case_number]
    for related_case in case_info['related_cases']:
        related_cases.add(related_case['case_number'])
related_cases = sorted(list(related_cases))
print('Constructed list of {} related cases.'.format(len(related_cases)))

for case_number in related_cases:
    if num_parses_left  == 0:
        break
    if case_number in updated_case_numbers:
        continue
    if (case_number in cases_save and
        cases_save[case_number]['status'] != 'Open'):
        print('Skipping related case {} because its status is {}'.format(
            case_number, cases_save[case_number]['status']))
        continue
    if case_number in cases_save:
        print('Updating related case {} ({} parses left)'.format(
            case_number, num_parses_left))
    else:
        print('Retrieving related case {} ({} parses left)'.format(
            case_number, num_parses_left))
    case_info = pynlrb.case(case_number)
    if case_info != None:
        updated_case_numbers[case_number] = True
        if case_number in cases_save and cases_save[case_number] == case_info:
            print('  Parse of case {} was identical to save.'.format(
                case_number))
        else:
            cases_save[case_number] = case_info
    num_parses_left -= 1    


# Spend the remaining parses on updating pre-existing parses.
# (Skip cases that are not still open.)
for case_number in random.sample(case_numbers_list, len(case_numbers_list)):
    if num_parses_left  == 0:
        break
    if case_number in updated_case_numbers:
        continue
    if (case_number in cases_save and
        cases_save[case_number]['status'] != 'Open'):
        print('Skipping case {} because its status is {}'.format(
            case_number, cases_save[case_number]['status']))
        continue
    print('Updating case {} ({} parses left)'.format(
        case_number, num_parses_left))
    case_info = pynlrb.case(case_number)
    if case_info != None:
        updated_case_numbers[case_number] = True
        if case_number in cases_save and cases_save[case_number] == case_info:
            print('  Parse of case {} was identical to save.'.format(
                case_number))
        else:
            cases_save[case_number] = case_info
    num_parses_left -= 1    


# Now save the updated cases.
with open(CASES_SAVE_FILE, 'w') as outfile:
    json.dump(cases_save, outfile, indent=1)
